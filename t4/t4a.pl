/*
    Problema: CD Independente
    Fonte: https://olimpiada.ic.unicamp.br/passadas/OBI2017/fase1/iniciacao/

    Uma banda formada por alunos e alunas da escola esta gravando um CD com exatamente sete músicas distintas – S, T, V, W, X, Y e Z. Cada música ocupa exatamente uma das sete faixas contidas no CD. Algumas das músicas são sucessos antigos de rock; outras são composições da própria banda. As seguintes restrições devem ser obedecidas:

    1 - S ocupa a quarta faixa do CD.
    2 - Tanto W como Y precedem S no CD (ou seja, W e Y estão numa faixa que e tocada antes de S no CD).
    3 - T precede W no CD (ou seja, T está numa faixa que ́e tocada antes de W).
    4 - Um sucesso de rock ocupa a sexta faixa do CD.
    5 - Cada sucesso de rock e imediatamente precedido no CD por uma composição da banda (ou seja, no CD cada sucesso de rock toca imediatamente após uma composição da banda).
    6 - Z e um sucesso de rock.
*/

%As regras podem ser expressas como:

%Quarta posição do Cd é ocupada por 'S'.
regra1(A) :-
    nth0(3,A,s).

%'W' e 'Y' precedem 'S'.
regra2(A) :-
    nth0(W,A,w),
    nth0(Y,A,y),
    nth0(S,A,s),
    W < S,
    Y < S.

%'T' precede 'W'.
regra3(A) :-
    nth0(W,A,w),
    nth0(T,A,t),
    T < W.

%'Z' é um sucesso do rock. Pelas regras 4 e 5, sucessos do rock ocupam as faixas pares, enquanto que as composições da banda ocupam as ímpares.
regra4(A) :-
    nth0(Z,A,z),
    mod(Z,2) =:= 1.


%Regras combinadas:

cdindependente(A) :-
    A = [_,_,_,_,_,_,_],
    Musicas = [s,t,v,w,x,y,z],
    permutation(Musicas,A),
    regra1(A),
    regra2(A),
    regra3(A),
    regra4(A).


/*
    Questão 11. Qual das seguintes alternativas poderia ser a ordem das musicas no CD, da primeira para a sétima faixa?

    (a) cdindependente([t,w,v,s,y,x,z]).
    (b) cdindependente([v,y,t,s,w,z,x]).
    (c) cdindependente([x,y,w,s,t,z,s]).
    (d) cdindependente([y,t,w,s,x,z,v]). ** Alternativa correta **
    (e) cdindependente([z,t,x,w,v,y,s]).
*/
