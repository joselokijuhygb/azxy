/*
    Problema: Popularidade de Páginas
    Fonte: https://olimpiada.ic.unicamp.br/passadas/OBI2014/fase2/iniciacao/

    Empresas de busca na internet, como Bing e Google, classificam as páginas da Internet de acordo com a sua “popularidade”. A popularidade de uma página X pode ser medida por exemplo pelo número de referências (links) de todas as outras páginas para X.  Estamos interessados em seis páginas – P, Q, R, S, T e U –, que têm popularidades diferentes entre si. As seguintes relações são conhecidas:

    1 - P é mais popular do que Q ou R, mas não mais popular do que ambas.
    2 - U é menos popular do que R.
    3 - Se Q é menos popular do que R, então nem S nem U são mais populares do que T.
    4 - Se Q é mais popular do que R, então S é mais popular do que ambas T e U.
*/

%As regras podem ser expressas como:

regra1(A) :-
    nth0(R,A,r),
    nth0(Q,A,q),
    nth0(P,A,p),
    ((P < Q , P > R) ; (P > Q , P < R)).

regra2(A) :-
    nth0(U,A,u),
    nth0(R,A,r),
    U > R.

regra3(A) :-
    nth0(R,A,r),
    nth0(Q,A,q),
    nth0(S,A,s),
    nth0(U,A,u),
    nth0(T,A,t),
    ((Q > R, S > T, U > T) ; (Q < R, S < T, S < U)).


%Regras combinadas:

popularidade(A) :-
    A = [_,_,_,_,_,_],
    Paginas = [p,q,r,s,t,u],
    permutation(Paginas, A),
    regra1(A),
    regra2(A),
    regra3(A).


/*
    Questão 4. Qual das seguintes poderia ser uma lista completa  e  correta  das  páginas,  ordenadas  da  mais popular para a menos popular?

    (a) popularidade([q,p,r,u,s,t]).
    (b) popularidade([s,r,p,t,q,u]).
    (c) popularidade([t,s,u,r,p,q]).
    (d) popularidade([r,t,p,s,q,u]). ** Alternativa correta **
    (e) popularidade([s,t,q,r,u,p]).

    Questão 5. Qual seguintes não poderia ser a página mais popular?

    (a) popularidade([r|_]).
    (b) popularidade([p|_]). ** Alternativa correta **
    (c) popularidade([s|_]).
    (d) popularidade([q|_]).
    (e) popularidade([t|_]).

    Pode-se usar, também, popularidade([X|_]).

    Questão 8. Cada uma das seguintes afirmativas poderia ser verdadeira exceto:

    (a) popularidade([q|_]).
    (b) popularidade([s|_]).
    (c) popularidade([_,t,_,_,_,_]).
    (d) popularidade([_,_,_,_,_,u]).
    (e) popularidade([_,u,_,_,_,_]). ** Alternativa correta **
*/
