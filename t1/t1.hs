import Data.Char

-- 01. Crie uma função isVowel :: Char -> Bool que verifique se um caracter é uma vogal ou não.

isVowel :: Char -> Bool
isVowel x = elem x "aeiouAEIOU"


-- 02. Escreva uma função addComma, que adicione uma vírgula no final de cada string contida numa lista.

addComma :: [String] -> [String]
addComma x = map (\y -> y ++ ",") x


-- 03. Crie uma função htmlListItems :: [String] -> [String], que receba uma lista de strings e retorne outra lista contendo as strings formatadas como itens de lista em HTML. Resolva este exercício COM e SEM funções anônimas (lambda).

-- com lambda

htmlListItems :: [String] -> [String]
htmlListItems x = map (\y -> "<li>" ++ y ++ "</li>") x

-- sem lambda

htmlListItems2 :: String -> String
htmlListItems2 x = "<li>" ++ x ++ "</li>"

htmlListItems3 :: [String] -> [String]
htmlListItems3 x = map htmlListItems2 x


-- 04. Defina uma função que receba uma string e produza outra retirando as vogais, conforme os exemplos abaixo. Resolva este exercício COM e SEM funções anônimas (lambda).

-- com lambda

retiravogais :: String -> String
retiravogais x = filter (\y -> isVowel y == False ) x

--sem lambda

retiravogais2 :: Char -> Bool
retiravogais2 x = isVowel x == False

retiravogais3 :: String -> String
retiravogais3 x = filter retiravogais2 x


-- 05. Defina uma função que receba uma string, possivelmente contendo espaços, e que retorne outra string substituindo os demais caracteres por '-', mas mantendo os espaços. Resolva este exercício COM e SEM funções anônimas (lambda).

--com lambda

subchar :: String -> String
subchar x = map (\y -> if y == ' ' then ' ' else '-') x

--sem lambda

subchar2 :: Char -> Char
subchar2 x 
 |x == ' ' = ' '
 |otherwise = '-'

subchar3 :: String->String
subchar3 x = map subchar2 x


-- 06. Escreva uma função firstName :: String -> String que, dado o nome completo de uma pessoa, obtenha seu primeiro nome. Suponha que cada parte do nome seja separada por um espaço e que não existam espaços no início ou fim do nome.

splitName :: Eq a => a -> [a] -> [[a]]
splitName d [] = []
splitName d s = x : splitName d (drop 1 y) where (x,y) = span (/= d) s

firstName :: String -> String
firstName x = head (splitName ' ' x)

firstName1 :: String -> String
firstName1 x = head $ splitName ' ' x

firstName2 :: String -> String
firstName2 x = head . splitName ' ' $ x

firstName3 :: String -> String
firstName3 x = takeWhile (/= ' ') x


-- 07. Escreva uma função isInt :: String -> Bool que verifique se uma dada string só contém dígitos de 0 a 9.

isInt :: String -> Bool
isInt ""  = False
isInt x  =
  case dropWhile isDigit x of
    ""       -> True
    _        -> False


-- 08. Escreva uma função lastName :: String -> String que, dado o nome completo de uma pessoa, obtenha seu último sobrenome. Suponha que cada parte do nome seja separada por um espaço e que não existam espaços no início ou fim do nome.

lastName :: String -> String
lastName x = last (splitName ' ' x)

lastName1 :: String -> String
lastName1 x = last $ splitName ' ' x

lastName2 :: String -> String
lastName2 x = last . splitName ' ' $ x


-- 09. Escreva uma função userName :: String -> String que, dado o nome completo de uma pessoa, crie um nome de usuário (login) da pessoa, formado por: primeira letra do nome seguida do sobrenome, tudo em minúsculas. Dica: estude as funções pré-definidas no módulo Data.Char, para manipulação de maiúsculas e minúsculas. Você precisará carregar este módulo usando import Data.Char no interpretador ou no início do arquivo do programa.

lowerString :: String -> String
lowerString str = [ toLower loweredString | loweredString <- str]

lowerString1 :: String -> String
lowerString1 = map toLower

lowerString2 :: String -> String
lowerString2 = \x -> map toLower x

userName :: String -> String
userName x = lowerString (showLitChar (head $ firstName x) (lastName x))

userName2 :: String -> String
userName2 x = lowerString ((show $ head $ firstName x) ++ (lastName x))

-- 10. Escreva uma função encodeName :: String -> String que substitua vogais em uma string, conforme o esquema a seguir: a = 4, e = 3, i = 2, o = 1, u = 0.

encodeChar :: Char -> Char
encodeChar x
  | x == 'a'  = '4'
  | x == 'e'  = '3'
  | x == 'i'  = '2'
  | x == 'o'  = '1'
  | x == 'u'  = '0'
  | otherwise = x

encodeName :: String -> String
encodeName x = map (\y -> encodeChar y) x

-- 11. Escreva uma função betterEncodeName :: String -> String que substitua vogais em uma string, conforme este esquema: a = 4, e = 3, i = 1, o = 0, u = 00.


--betterEncodeName :: String -> String
--betterEncodeName :: String -> String
--betterEncodeName x = map (\y -> if y == 'u' then 'u':y else encodeChar y) x

-- 12. Dada uma lista de strings, produzir outra lista com strings de 10 caracteres, usando o seguinte esquema: strings de entrada com mais de 10 caracteres são truncadas, strings com até 10 caracteres são completadas com '.' até ficarem com 10 caracteres.

truncateString :: [String] -> [String]
truncateString x = map (\y -> if length y >= 10 then take 10 y else y ++ (take (10 -length y) (repeat '.'))) x
