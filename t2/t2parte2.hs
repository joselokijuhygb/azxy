import Data.Char -- Usado no item 6.

-- 1. Escreva uma função recursiva isBin :: String -> Bool para verificar se uma dada String representa um número binário, ou seja, contém apenas caracteres '0' ou '1'. As únicas funções pré-definidas autorizadas aqui são head e tail.

isBin :: String -> Bool
isBin [] = True
isBin str = if (head str) == '0' || (head str) == '1' then isBin (tail str) else False

isBin2 :: String -> Bool
isBin2 [] = True
isBin2 (x:xs) = if x == '0' || x == '1' then isBin2 xs else False

isBin3 :: String -> Bool
isBin3 [] = True
isBin3 (x:xs) = if elem x "01" then isBin3 xs else False


-- 2. Reescreva a função acima de forma não-recursiva. Dê outro nome para ela, por exemplo isBin'. Aqui você pode usar quaisquer funções auxiliares pré-definidas em Haskell.

isBin4 :: String -> Bool
isBin4 str = null $ filter (\y -> y /= '0' && y /= '1') str

isBin5 :: String -> Bool
isBin5 str = null [x | x <- str, notElem x "01" == True]


-- 3. Encontra-se abaixo a definição parcial da função bin2dec :: [Int] -> Int, que converte uma lista de 0's e 1's (representando um número binário), em seu equivalente em decimal. Implemente a função auxBin2Dec de forma recursiva, para que bin2dec funcione corretamente, conforme os exemplos.

auxBin2Dec :: [Int]-> Int -> Int
auxBin2Dec [0] 0 = 0
auxBin2Dec [1] 0 = 1
auxBin2Dec (x:xs) y = x*2^y + auxBin2Dec xs (y-1)

bin2dec :: [Int] -> Int
bin2dec [] = undefined
bin2dec bits = auxBin2Dec bits (length bits-1)


-- 4. Reescreva a função do exercício anterior de forma não-recursiva, usando funções pré-definidas em Haskell. Dê outro nome para a função (por exemplo, bin2dec').

-- A função recebe dois parâmetros. O segundo é a base. Assim, dá pra usar a função para conversão hex2dec, alem de bin2dec.
binhex2dec :: [Int] -> Int -> Int
binhex2dec [] x = undefined
binhex2dec bits y = sum $ zipWith (*) bits [y^x | x <- [length bits -1,length bits -2..0]]


-- 5. Crie uma função recursiva dec2bin :: Int -> [Int] que receba um número inteiro positivo e retorne sua representação em binário, sob forma de uma lista de 0's e 1's. As funções auxiliares autorizadas aqui são mod, div e reverse.

dec2aux :: Int -> Int -> [Int]
dec2aux x y
    | x < y = [x]
    | x >= y = (x `mod` y):dec2aux (x `div` y) y

dec2bin :: Int -> [Int]
dec2bin x = reverse $ dec2aux x 2


-- 6. Implemente uma dessas funções: isHex :: String -> Bool ou hex2dec :: String -> Int ou dec2hex :: Int -> String, que são semelhantes às dos exercícios anteriores, porém com números hexadecimais no lugar de números binários. Aqui está tudo liberado: você pode escolher qual das funções irá implementar, sem restrições sobre como deve fazer isso.

isHex :: String -> Bool
isHex str = null $ filter (\y -> not $ elem y "0123456789abcdefABCDEF") str

dec2hex :: Int -> String
dec2hex x = map ((['0'..'9'] ++ ['a'..'z']) !!) (reverse $ dec2aux x 16)

hex2dec :: String -> Int
hex2dec str = binhex2dec (map digitToInt str) 16
