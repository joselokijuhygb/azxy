-- A codificação EAN-13 é um padrão de código de barras usado em vários tipos de produtos. O número codificado em barras tem 13 dígitos (0-9), sendo o último um dígito verificador (exemplo: 5901234123457, dígito verificador 7). O cálculo do dígito verificador obedece a algumas regras simples disponíveis em: https://www.gs1.org/services/how-calculate-check-digit-manually

-- Você deverá implementar uma função isEanOk :: String -> Bool, que verifique se uma dada string representa um número EAN-13 com dígito verificador válido.

import Data.Char

eanDV :: [Int] -> Int
eanDV digits = 10 - ((sum $ zipWith (*) digits [1,3,1,3,1,3,1,3,1,3,1,3]) `mod` 10)

isEanOk :: String -> Bool
isEanOk str = (eanDV $ map digitToInt str) == digitToInt (last str)


isEanOk2 :: String -> Bool
isEanOk2 str = 10 - ((sum $ zipWith (*) (map digitToInt str) (take 12 (cycle [1,3]))) `mod` 10) == digitToInt (last str)
