-- 1. Usando recursão, escreva uma função geraTabela :: Int -> [(Int,Int)] que produza uma lista com n tuplas, cada tupla com números de n a 1 e seus respectivos quadrados.

-- Com recursão:
geraTabela :: Int -> [(Int,Int)]
geraTabela 1 = [(1,1)]
geraTabela x = (x,x^2):geraTabela (x-1)

-- Sem recursão:
geraTabela2 :: Int -> [(Int,Int)]
geraTabela2 x = [(p,p*p) | p <- [x,(x-1)..1]]


-- 2. Defina uma função recursiva que verifique se um dado caracter está contido numa string.

-- Com recursão e sem x:xs:
contido :: Char -> String -> Bool
contido c [] = False
contido c str = if c == head str then True else contido c (tail str)

-- Com recursão e com x:xs:
contido2 :: Char -> String -> Bool
contido2 c [] = False
contido2 c (x:xs) = if c == x then True else contido2 c xs

--Sem recursão:
contido3 :: Char -> String -> Bool
contido3 c str = not . null $ filter (\y -> y == c) str


-- 3. Defina uma função recursiva que receba uma lista de coordenadas de pontos 2D e desloque esses pontos em 2 unidades.

-- Com recursão
translate :: [(Float,Float)] -> [(Float,Float)]
translate [] = []
translate (x:xs) = (fst (x) + 2, snd (x) + 2):translate (xs)

-- Sem recursão
translate2 :: [(Float,Float)] -> [(Float,Float)]
translate2 lst = [(a+2,b+2) | (a,b) <- lst]

-- 4. Defina uma função que receba um número n e retorne uma lista de n tuplas, cada tupla com números de 1 a n e seus respectivos quadrados. Dica: defina uma função auxiliar recursiva com 2 argumentos, sendo um deles que se mantém inalterado na chamada recursiva.

-- Com recursão
geraTabela3aux :: Int -> Int -> [(Int,Int)]
geraTabela3aux 0 y = []
geraTabela3aux x y = (y-x+1,(y-x+1)^2):geraTabela3aux (x-1) y

geraTabela3 :: Int -> [(Int,Int)]
geraTabela3 x = geraTabela3aux x x

geraTabela4 :: Int -> [(Int,Int)]
geraTabela4 x = reverse $ geraTabela x

-- Sem recursão:
geraTabela5 :: Int -> [(Int,Int)]
geraTabela5 x = [(p,p*p) | p <- [1..x]]
