% 1. Defina um predicado zeroInit(L) que é verdadeiro se L for uma lista que inicia com o número 0 (zero).

zeroInit([M|_]):-
    member(0,[M]).

zeroInitAlt([M|_]):-
    M=:=0.


% 2. Defina um predicado has5(L) que é verdadeiro se L for uma lista de 5 elementos. Resolva este exercício sem usar um predicado auxiliar.

has5([_,_,_,_,_]).

has5Alt(R):-
    length(R,5).


% 3. Defina um predicado hasN(L,N) que é verdadeiro se L for uma lista de N elementos.

hasN(R,M):-
    length(R,M).


% 4. Defina um predicado potN0(N,L), de forma que L seja uma lista de potências de 2, com expoentes de N a 0.

potN0(0,[1]):- !.
potN0(M,[N|R]):-
    N is 2**M,
    O is M-1,
    potN0(O,R).


% 5. Defina um predicado zipmult(L1,L2,L3), de forma que cada elemento da lista L3 seja o produto dos elementos de L1 e L2 na mesma posição do elemento de L3.

zipmult([],[],[]):- !.
zipmult([M|R],[N|S],[O|T]):-
    O is M*N,
    zipmult(R,S,T).


% 6. Defina um predicado potencias(N,L), de forma que L seja uma lista com as N primeiras potências de 2, sendo a primeira 2^0 e assim por diante, conforme o exemplo abaixo:

potencias(0,[]):- !.
potencias(M,R):-
    potN0(M-1,S),
    inverte(S,R).

% Predicado para inverter uma lista.
inverte(R,S):-
    inverteAux([],R,S).

inverteAux(T,[],T):- !.
inverteAux(T,[R|S],U):-
    inverteAux([R|T],S,U).


% 7\. Defina um predicado positivos(L1,L2), de forma que L2 seja uma lista só com os elementos positivos de L1, conforme o exemplo abaixo:

positivos([],[]).
positivos([M|R],[M|S]):-
    M>=0,
    positivos(R,S).
positivos([_|R],S):-
    positivos(R,S).

% Predicado que separa uma lista de números em listas de negativos e positivos.
dividir([],[],[]).
dividir([M|R],S,[M|T]):-
    M>=0,
    dividir(R,S,T).
dividir([M|R],[M|S],T):-
    dividir(R,S,T).


% 8. Considere que L1 e L2 sejam permutações de uma lista de elementos distintos, sem repetições. Sabendo disso, defina um predicado mesmaPosicao(A,L1,L2) para verificar se um elemento A está na mesma posição nas listas L1 e L2.

mesmaPosicao(M,R,S):-
    posicao(N,M,R),
    posicao(O,M,S),
    N=:=O.

% Predicado que retorna a posição de um elemento numa lista.
posicao(0,M,[M|_]):- !.
posicao(N,M,[_|R]):-
    posicao(O,M,R),
    N is O + 1.


% 9. Dada uma lista de N alunos, deseja-se escolher NP alunos (NP < N) para formar uma comissão. Para isso, defina um predicado comissao(NP,LP,C), que permita gerar as possíveis combinações C com NP elementos da lista LP.

comissao(0,_,[]).
comissao(M,[N|R],[N|S]):-
    M>0,
    O is M-1,
    comissao(O,R,S).
comissao(M,[_|R],S):-
    M>0,
    comissao(M,R,S).


% 10. (Adaptado de OBI2006-F1N1) Tem-se N azulejos 10cm x 10cm e, com eles, deve-se montar um conjunto de quadrados de modo a utilizar todos os azulejos dados, sem sobrepô-los. Inicialmente, deve-se montar o maior quadrado possível; então, com os azulejos que sobraram, deve-se montar o maior quadrado possível, e assim sucessivamente. Por exemplo, se forem dados 31 azulejos, o conjunto montado terá 4 quadrados. Para resolver este problema, você deverá definir um predicado azulejos(NA, NQ), de forma que NQ seja o número de quadrados que se deve montar com NA azulejos. Dica: use os predicados sqrt e floor, pré-definidos em Prolog.

azulejos(M,N):-
    azulejosAux(M,O),
    length(O,N).

% Predicado que retorna a lista decrescente com os lados dos maiores quadrados.
azulejosAux(0,[]):- !.
azulejosAux(M,[N|R]):-
    sqrt(M,O),
    floor(O,N),
    P is M-N**2,
    azulejosAux(P,R).
