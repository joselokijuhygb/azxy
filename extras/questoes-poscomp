ENADE

Questão 18. Os números de Fibonacci constituem uma seqüência de números na qual os dois primeiros elementos são 0 e 1 e os demais, a soma dos dois elementos imediatamente anteriores na seqüência. Como exemplo, a seqüência formada pelos 10 primeiros números de Fibonacci é: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34. Mais precisamente, é possível definir os números de Fibonacci pela seguinte relação de recorrência:

fib (n) = 0, se n = 0
fib (n) = 1, se n = 1
fib (n) = fib (n - 1) + fib (n - 2), se n > 1

Abaixo, apresenta-se uma implementação em linguagem funcional para essa relação de recorrência:

fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

Considerando que o programa acima não reutilize resultados previamente computados, quantas chamadas são feitas à função fib para computar fib 5?

Resposta: Usando a definição recursiva, fib(5) é calculado assim:

fib(5) = fib(4) + fib(3) = fib(3) + fib(2) + fib(2) + fib(1) = fib(2) + fib(1) + fib(1) + fib(0) + fib(1) + fib(0) + 1 = fib(1) + fib(0) + 4 = 1 + 4 = 5

Assim, a função fib é chamada 15 vezes para computar fib(5).


==============================================================

POSCOMP 2005

Questão 29. Considere as seguintes afirmações:
(I) O paradigma da programação funcional é baseado em funções matemáticas e composição de funções.
(II) prolog é uma linguagem de programação cuja sintaxe é uma versão simplificada do cálculo de predicados e seu método de inferência é uma forma restrita de Resolução.
(III) O conceito de “Classe” foi primeiramente introduzido por Simula67.
(IV) O paradigma orientado a objeto surgiu em paralelo ao desenvolvimento de Smalltalk.
(V) No paradigma declarativo, programas são expressos na forma de lógica simbólica e usam um processo de inferência lógica para produzir resultados.

Quais são as afirmações verdadeiras?
(a) Somente (I) e (V).
(b) Somente (II) e (V).
(c) Somente (I), (II) e (V).
(d) Somente (I) e (II).
(e) Todas as afirmações são verdadeiras. ***

Resposta: todas as afirmativas sobre as linguagens estão corretas, alternativa E.

==============================================================

POSCOMP 2006

Questão 63. [TE] Considere o programa Prolog:

blabla([ ],L,L).
blabla([X|L1],L2,[X|L3]):- blabla(L1,L2,L3).

Quantas possíveis respostas a interrogação abaixo fornece (considerando o backtracking)?

?- blabla(L1,L2,[a,b]).

(a) 1
(b) 2
(c) 3 ***
(d) 4
(e) 5

Resposta: O algoritmo de execução do prolog faz uma busca em profundidade em numa arvore de pesquisa, com resolução de um objetivo, onde a lista de clausulas (fatos e regras) é pesquisada de cima para baixo. Como uma das clausulas é uma regra recursiva e a lista é [a,b] existirão 4 possíveis caminhos na árvore, onde apenas o mais a direita terá uma tentativa de unificação não bem sucedida. Por isso as três as possíveis respostas à ?- blabla(L1,L2,[a,b]). são dada nesse ordem:

?- blabla(L1,L2,[a,b]).
L1 = [],
L2 = [a, b] ;
L1 = [a],
L2 = [b] ;
L1 = [a, b],
L2 = [] ;
false.


==============================================================

POSCOMP 2007

Questão 28. [FU] Considere o seguinte enunciado e as possibilidades de sua complementação. A regra de inferência utilizada pela linguagem Prolog, denominada “regra de resolução”,

I. opera com fórmulas contendo apenas quantificadores existenciais.
II. é capaz de reduzir fórmulas quantificadas à suas correspondentes formas clausais.
III. opera sobre fórmulas em forma clausal pelo corte de literais de sinais opostos.
IV. opera sobre fórmulas em forma clausal pelo corte de literais de mesmo sinal.
V. produz deduções que evitam a construção de árvores de dedução lineares.

Completa(m) CORRETAMENTE o enunciado acima

(a) apenas o item II.
(b) apenas o item III. ***
(c) apenas o item IV.
(d) apenas os itens I e II.
(e) apenas os itens III e V.

Resposta: a regra de inferência utilizada pela Prolog opera com fórmulas contendo quantificadores universais, sobre fórmulas em forma clausal pelo corte de literais de sinais opostos, não é capaz de reduzir fórmulas quantificadas em suas correspondentes formas clausais e constrói árvores de dedução lineares. Daí, a alternativa que completa o enunciado é a B.

==============================================================

POSCOMP 2008

Questão 22. Os fragmentos de programas abaixo, enumerados 1, 2 e 3, são implementações para o problema de ordenação usando o algoritmo quicksort.

Programa 1:
quicksort([], []).
quicksort([Head | Tail], Sorted) :-
    partition(Head, Tail, Left, Right), quicksort(Left, SortedL),
    quicksort(Right, SortedR),
    append(SortedL, [Head | SortedR], Sorted).
partition(Pivot, [], [], []).
partition(Pivot, [Head | Tail], [Head | Left], Right) :-
    Head =< Pivot, partition(Pivot, Tail, Left, Right).
partition(Pivot, [Head | Tail], Left, [Head | Right]) :-
    Head > Pivot, partition(Pivot, Tail, Left, Right).
append([], List, List).
append([Head | List1], List2, [Head | List3]) :-
    append(List1, List2, List3).

Programa 2:
quicksort [] = []
quicksort (head:tail) = let pivot = head
left = [x|x <- tail,x < pivot]
right = [x|x <- tail,x >= pivot]
in quicksort left ++ [pivot] ++ quicksort right

Programa 3:
void quickSort( int a[], int l, int r) {
    int j;
    if( l < r ) {
        j = partition( a, l, r);
        quickSort( a, l, j-1);
        quickSort( a, j+1, r);
    }
}
int partition( int a[], int l, int r) {
    int pivot, i, j, t;
    pivot = a[l]; i = l; j = r+1;
    while(i<j) {
        do ++i; while( a[i] <= pivot && i <= r );
        do --j; while( a[j] > pivot );
        if( i < j ) {
            t = a[i]; a[i] = a[j]; a[j] = t;
        }
    }
    t = a[l]; a[l] = a[j]; a[j] = t;
    return j;
}

Assinale a alternativa que enumera os paradigmas das linguagens com as quais os programas 1, 2 e 3 foram respectivamente implementados.

A) Lógico, imperativo e funcional
B) Imperativo, funcional e lógico
C) Funcional, lógico e imperativo
D) Lógico, funcional e imperativo ***
E) Funcional, funcional e imperativo

Resposta: O programa 1 é escrito na linguagem prolog, de paradigma lógico. O programa 2 é escrito em Haskell, de paradigma funcional. E o programa 3 é escrito em C, de paradigma imperativo. Portanto, a alternativa D enumera os paradigmas das linguagens com as quais os programas 1, 2 e 3 foram escritos.


==============================================================

POSCOMP 2008

Questão 23. Analise as seguintes afirmativas.

I. A função map presente em linguagens funcionais como Haskell e OCaml é um bom exemplo de função de alta-ordem com tipo polimórfico.
II. Prolog é uma linguagem de programação baseada em lógica de predicados de primeira ordem.
III. Em Haskell todas as funções recebem apenas um argumento. Uma função que recebe dois inteiros e devolve um float como resposta na verdade é uma função que recebe apenas um inteiro como argumento e devolve como resposta uma função de inteiro para float.

A análise permite concluir que

A) apenas as afirmativas II e III estão corretas.
B) apenas as afirmativas I e III estão corretas.
C) apenas as afirmativas I e II estão corretas.
D) apenas a afirmativa II está correta.
E) apenas as afirmativas I, II e III estão corretas. ***

Resposta: todas as afirmativas sobre as linguagens estão corretas, alternativa E.


==============================================================

POSCOMP 2009

Questão 47. [FUN]
Seja o programa em Prolog a seguir:

pai(abel,bernardo).
pai(abel,bia).
mae(ana,bernardo).
mae(ana,bia).
parenteSimples(X,Y) :- pai(X,Y).
parenteSimples(X,Y) :- mae(X,Y).
irmao(X,Y) :- parenteSimples(Z,X),parenteSimples(Z,Y),X\=Y.

Qual a resposta para a entrada: irmao(X,Y).
Supondo que para cada resposta do programa é digitado “;” (ponto e vírgula).

A) X = bernardo,
Y = bia ;
X = bia,
Y = bernardo ;
false.

B) X = bernardo,
Y = bia ;
X = bernardo,
Y = bia ;
X = bia,
Y = bernardo ;
X = bia,
Y = bernardo ;
false.

C) X = bernardo, ***
Y = bia ;
X = bia,
Y = bernardo ;
X = bernardo,
Y = bia ;
X = bia,
Y = bernardo ;
false.

D) X = bernardo,
Y = bia ;
false.

E) Nenhuma das respostas anteriores.

Resposta: os fatos e regras são pesquisados de cima pra baixo e da esquerda para a direita, ao consultar irmao(X,Y). Como primeiro se procura pessoas com pai e depois com mãe, as opções para X são dadas na ordem em que aparecem na base de fatos. Para Y, as opções tem que estar na base de fatos e ser diferentes das de X. Então, se X é bernardo, Y será bia, se X é bia, Y é bernardo. Portanto a resposta para a entrada irmao(X,Y). é a alternativa C.


==============================================================

POSCOMP 2009

Questão 44. Dada a seguinte expressão em LISP, qual o seu resultado?

(CAR (CDR (CDR '( A B C D E ))))

(a) A
(b) B
(c) C ***
(d) D
(e) nil

Resposta: A expressão em LISP retorna C. Os dois primeiros CDR removem os dois primeiros elementos da lista ( A B C D E ), retornando a lista (C D E). E CAR retorna o primeiro elemento dessa lista, C.


==============================================================

POSCOMP 2010

Questão 28. Com base nos conhecimentos sobre as linguagens de programação funcionais, considere as afirmativas a seguir.

I. Uma linguagem de programação funcional tem o objetivo de imitar as funções matemáticas, ou seja,
os programas são definições de funções e de especificações da aplicação dessas funções.
II. Nas linguagens funcionais, os dados e as rotinas para manipulá-los são mantidos em uma mesma
unidade, chamada objeto. Os dados só podem ser manipulados por meio das rotinas que estão na
mesma unidade.
III. As rotinas de um programa do paradigma funcional descrevem ações que mudam o estado das variá-
veis do programa, seguindo uma sequência de comandos para o computador executar.
IV. A linguagem Lisp é um exemplo do paradigma funcional de programação.

Assinale a alternativa correta.

a) Somente as afirmativas I e IV são corretas. ***
b) Somente as afirmativas II e III são corretas.
c) Somente as afirmativas III e IV são corretas.
d) Somente as afirmativas I, II e III são corretas.
e) Somente as afirmativas I, II e IV são corretas.

Resposta: apenas I e IV estão corretas. II se refere a linguagem orientada a objetos e III a linguagens imperativas. Alternativa A.

==============================================================

POSCOMP 2011

Questão 35. Com relação aos Paradigmas de Linguagens de Programação e as linguagens apresentadas na segunda coluna abaixo, relacione a primeira coluna com a segunda considerando a linguagem que melhor representa cada paradigma.

(I) Programação Imperativa
(II) Programação Orientada a Objetos
(III) Programação Funcional
(IV) Programação Lógica

(A) Linguagem Scheme
(B) Linguagem Smalltalk
(C) Linguagem Pascal
(D) Linguagem Prolog

Assinale a alternativa que contém a associação correta.

a) I-A, II-B, III-D, IV-C.
b) I-B, II-A, III-C, IV-D.
c) I-C, II-A, III-B, IV-D.
d) I-C, II-B, III-A, IV-D. ***
e) I-D, II-C, III-B, IV-A.

Resposta: Prolog usa paradigma lógico, Pascal o imperativo, Smalltalk o orientado a objetos e Scheme o funcional. Alternativa D.

==============================================================

POSCOMP 2012

Questão 32. Em linguagens de programação declarativas, em especial aquelas que seguem o paradigma funcional, a lista é uma estrutura de dados fundamental. Uma lista representa coleções de objetos de um único tipo, sendo composta por dois elementos: a cabeça (head) e o corpo (tail), exceto quando está vazia. A cabeça é sempre o primeiro elemento e o corpo é uma lista com os elementos da lista original, excetuando-se o primeiro elemento. O programa Haskell, a seguir, apresenta uma função que utiliza essa estrutura de dados.
poscomp :: [Int] -> [Int]
poscomp [] = []
poscomp [x] = [x]
poscomp (a:b:c) | a > b = b : (a : poscomp c)
                | otherwise = a : (b : poscomp c)

Uma chamada a esta função através da consulta
poscomp [5,3,4,5,2,1,2,3,4]
produzirá o resultado:

a) [1,2,2,3,3,4,4,5,5]
b) [3,5,4,5,1,2,2,3,4] ***
c) [5,3,4,5,2,1,2,3,4]
d) [5,4,3,2,1]
e) [5,3,4,2,1]

Resposta: O programa compara um elemento numa posição ímpar da lista (supondo que o primeiro elemento da lista está na posição 1) com o elemento na posição seguinte (caso ela existir), invertendo as posições se for maior, caso contrário, mantendo-a. Assim, a consulta poscomp [5,3,4,5,2,1,2,3,4] produzirá o resultado [3,5,4,5,1,2,2,3,4], alternativa B.


==============================================================

POSCOMP 2015

Questão 27. A linguagem de programação LISP usa o paradigma de:

(A) programação procedural.
(B) programação de tipos abstratos de dados.
(C) programação orientada a objetos.
(D) programação funcional. ***
(E) programação declarativa.

Resposta: LISP é uma linguagem que usa o paradigma de programação funcional, alternativa D.
