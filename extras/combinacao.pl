% 9. Dada uma lista de N alunos, deseja-se escolher NP alunos (NP < N) para formar uma comissão. Para isso, defina um predicado combinacao(NP,LP,C), que permita gerar as possíveis combinações C com NP elementos da lista LP.

combinacao(0,_,[]).
combinacao(M,[N|R],[N|S]):-
    M>0,
    O is M-1,
    combinacao(O,R,S).
combinacao(M,[_|R],S):-
    M>0,
    combinacao(M,R,S).


% Usar, por exemplo, comb2([1,2,3,4],[X,Y]) para gerar combinações com 2 elementos.

combinacao2(_,[]).
combinacao2([M|R],[M|S]):-
    combinacao2(R,S).
combinacao2([_|R],[M|S]):-
    combinacao2(R,[M|S]).

