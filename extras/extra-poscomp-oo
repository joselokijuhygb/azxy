EXTRA - Resolução de 17 questões relacionadas à OO.

*********************

POSCOMP 2005 - QUESTÃO 29
Considere as seguintes afirmações:

I. O paradigma da programação funcional é baseado em funções matemáticas e composição de funções.
II. Prolog é uma linguagem de programação cuja sintaxe é uma versão simplificada do cálculo de predicados e seu método de inferência é uma forma restrita de Resolução.
III. O conceito de “Classe” foi primeiramente introduzido por Simula67.
IV. O paradigma orientado a objeto surgiu em paralelo ao desenvolvimento de Smalltalk.
V. No paradigma declarativo, programas são expressos na forma de lógica simbólica e usam um processo de inferência lógica para produzir resultados.

Quais são as afirmações verdadeiras?

A) Somente I e V.
B) Somente II e V.
C) Somente I, II e V.
D) Somente I e II.
E) Todas as afirmações são verdadeiras. ***

Resposta: Todas as afirmações são verdadeiras, alternativa correta é a letra "E".

*********************

POSCOMP 2007 - QUESTÃO 29
Analise as seguintes afirmativas.

I. Encapsulamento é a capacidade de uma operação atuar de modos diversos em classes diferentes.
II. Polimorfismo é o compartilhamento de atributos e métodos entre classes com base em um relacionamento hierárquico.
III. Herança consiste no processo de ocultação dos detalhes internos de implementação de um objeto.
IV. Sobreposição é a redefinição das funções de um método herdado. Os métodos apresentam assinaturas iguais.
V. Em JAVA, todos os métodos numa classe abstrata devem ser declarados como abstratos.

A partir da análise, pode-se concluir que

A) apenas a afirmativa IV está correta.    ***
B) apenas as afirmativas III e IV estão corretas.
C) apenas as afirmativas I, IV e V estão corretas.
D) apenas as afirmativas I, III e V estão corretas.
E) todas as afirmativas são falsas.

Resposta: a afirmativa I, na verdade, se refere ao conceito de polimorfismo ao invés do de encapsulamento. A II, ao de herança, ao invés do de polimorfismo. A III, ao de encapsulamento ao invés do de herança.
Já a afirmativa V não é verdade, pois uma classe abstrata pode ter métodos abstratos ou concretos. Se todos os métodos forem abstratos, então será uma interface. A alternativa correta é a "A", que trata da sobreposição, onde métodos herdados podem ser redefinidos.

*********************

POSCOMP 2008 - QUESTÃO 20
Analise as seguintes afirmativas.

I. Ocultar dados dentro das classes e torná-los disponíveis apenas por meio de métodos é uma técnica muito usada em programas orientados a objetos e é chamada de sobrescrita de atributos.
II. Uma subclasse pode implementar novamente métodos que foram herdados de uma superclasse. Chamamos isso de sobrecarga de métodos.
III. Em Java não existe Herança múltipla como em C++. A única maneira se se obter algo parecido é via interfaces.

A análise permite concluir que

A) apenas a afirmativa I está incorreta.
B) apenas a afirmativa II está incorreta.
C) apenas a afirmativa III está incorreta.
D) apenas as afirmativas I e III estão incorretas.
E) apenas as afirmativas I e II estão incorretas    ***

Resposta: a afirmativa I se refere ao conceito de encapsulamento e não à "sobrescrita de atributos". Já a II se refere, na verdade, ao polimorfismo. E a afirmativa III está correta, assim, a alternativa correta é a letra "E".

*********************

POSCOMP 2008 - QUESTÃO 21
Analise as seguintes afirmativas.

I. Encapsulamento permite que uma classe defina métodos com o mesmo nome de métodos presentes em sua superclasse desde que esses métodos tenham argumentos um pouco diferentes.
II. Em Java, uma instância de uma classe C que implementa uma interface I é membro tanto do tipo definido pela interface I quanto do tipo definido pela classe C.
III. Em Java, classes abstratas não precisam ser completamente abstratas, ao contrário das interfaces, classes abstratas podem ter métodos implementados que serão herdados por suas subclasses.

A análise permite concluir que

A) apenas as afirmativas II e III estão corretas.   ***
B) apenas as afirmativas I e II estão corretas.
C) apenas as afirmativas I e III estão corretas.
D) apenas a afirmativa II está correta.
E) apenas a afirmativa I está correta.

Resposta: a afirmativa I se refere, na verdade à sobrecarga de métodos, portanto está errada. Como uma instância de uma classe que implementa um interface é membro dos tipos definidos pela classe e interface e como classes abstratas não precisam ser completamente abstratas, as alternativas II e III estão corretas. Assim, a alternativa correta é a letra "A".

*********************

POSCOMP 2008 - QUESTÃO 41
Os membros de uma classe (atributos e operações) podem ser privados, protegidos ou públicos em programação orientada a objetos. Suponha agora que se tenha um dado em uma determinada classe que só deve ser acessado por instâncias dessa mesma classe.

Assinale a alternativa que melhor descreve o que esse dado pode ser.

A) Somente público
B) Somente privado
C) Somente protegido
D) Privado ou público
E) Privado ou protegido

Resposta: como membros públicos ou protegidos são visíveis por classes e subclasses no mesmo pacote a melhor opção é tornar o dado somente privado. Portanto a alternativa correta é a letra "B".

*********************

POSCOMP 2009 - QUESTÃO 37
Considere as afirmativas abaixo:

I. Fortran, Pascal e Java são linguagens de terceira geração.
II. C++ e Java permitem a criação de classes e o uso de herança múltipla.
III. Prolog é uma linguagem funcional pura.
IV. PHP, Perl e Ruby são linguagens de sexta geração.

Assinale a alternativa CORRETA:

A) Apenas a afirmativa I é verdadeira.  ***
B) Apenas a afirmativa II é verdadeira.
C) Apenas a afirmativa III é verdadeira.
D) Apenas as afirmativas I e IV são verdadeiras.
E) Apenas as afirmativas II e III são verdadeiras.

Resposta: Prolog não é funcional pura. Java não permite herança múltipla. PHP, Perl e Ruby não são de sexta geração. Como Fortran, Pascal e Java são de terceira geração, a alternativa correta é a "A".

*********************

POSCOMP 2009 - QUESTÃO 46
Qual o resultado do programa em Java a seguir:

public class Prova
{
    static int v1;
    int v2;
    static { v1=1 ;}
    { v2 = 2; }
    void troca()
    {
        v1=v2 ;
    }
    public static void main(String[] args)
    {
        Prova a=new Prova();
        Prova b=new Prova();
        a.v2=5;
        a.troca();
        System.out.print(a.v1);
        System.out.print(a.v2);
        System.out.print(b.v1);
        System.out.print(b.v2);
    }
}

A) 1522
B) 5512
C) 1512
D) 5552 ***
E) Nenhuma das respostas anteriores.

Resposta: Inicialmente os objetos possuem v1=1 e v2=2. Como v1 é static, qualquer alteração vale para todos os objetos. Assim inicialmente a.v1 e b.v1 são 1, a.v2 e b.v2 são 2, com a alteração de a.v2 para 5 e de v1 para 5, por meio do método troca, teremos que a.v1, a.v2 e b.v1 são 5 e b.v2 continua 2. Assim o resultado do programa é 5552. Alternativa correta é a letra "D".

*********************

POSCOMP 2010 - QUESTÃO 30
O mecanismo de herança, no paradigma da programação orientada a objetos, é uma forma de reutilização de software na qual uma nova classe é criada, absorvendo membros de uma classe existente e aprimorada com capacidades novas ou modificadas.

Considere as seguintes classes descritas na linguagem C++.

#include <iostream>
using namespace std;
class A
{
    protected:
    int v;

    public:
    A() { v = 0; };
    void m1()
    {
        v += 10;
        m2();
    };

    void m2()
    {
        v += 20;
    };

    int getv()
    {
        return v;
    };
};

class B : public A
{
    public:
    void m2()
    {
        v += 30;
    };
};

Se essas classes forem utilizadas a partir do programa a seguir,

int main()
{
    B *Obj = new B();
    Obj->m1();
    Obj->m2();
    cout << Obj->getv() << endl;
    return 0;
}

a saída do código computacional acima será:

A) 30
B) 40
C) 50
D) 60   ***
E) 70

Resposta: usando o conceito de herança, v é inicialmente inicializado com 0, o método m1 da classe A adiciona 10 à v, depois o método m2 da mesma classe adiciona 20 e, por fim, o método m2 da classe B adiciona 30. Assim, a saída do código será 60, sendo a alternativa correta a letra "D".

*********************

POSCOMP 2011 - QUESTÃO 34
Em linguagens orientadas a objetos, o polimorfismo refere-se à ligação tardia de uma chamada a uma ou várias implementações diferentes de um método em uma hierarquia de herança.

Neste contexto, considere as seguintes classes descritas na Linguagem C++.

#include <iostream>
using namespace std;
class PosComp1
{
    public:
    int Calcula()
    { return 1; };
};

class PosComp2 : public PosComp1
{
    public:
    virtual int Calcula()
    { return 2; }
};

class PosComp3 : public PosComp2
{
    public:
    int Calcula()
    { return 3; }
};

Se estas classes forem utilizadas a partir do programa a seguir

int main()
{
    int Result=0;
    PosComp1 *Objs[3];
    Objs[0] = new PosComp1();
    Objs[1] = new PosComp2();
    Objs[2] = new PosComp3();
    for (int i=0; i<3; i++)
        Result += Objs[i]->Calcula();
    cout << Result << endl;
    return 0;
}

a saída desse programa será:

A) 0
B) 3    ***
C) 5
D) 6
E) 9

Resposta: o principal a observar aqui é que na superclasse PosComp1, o método Calcula não possui o modificador "virtual". Assim, as classes filhas não podem sobrepor ele. Portanto, sempre que Calcula for chamado, retorna 1. Portanto a saída do programa será 3, alternativa "B". Se o método Calcula possuir o modificador "virtual" a saída do programa será 6.

*********************

POSCOMP 2012 - QUESTÃO 30
O encapsulamento dos dados tem como objetivo ocultar os detalhes da implementação de um determinado módulo. Em linguagens orientadas a objeto, o ocultamento de informação é tornado explícito requerendo-se que todos os métodos e atributos em uma classe tenham um nível particular de visibilidade com relação às suas subclasses e às classes clientes.

Em relação aos atributos de visibilidade, assinale a alternativa correta.

A) Um atributo ou método público é visível a qualquer classe cliente e subclasse da classe a que ele pertence.
B) Um atributo ou método protegido é visível somente à classe a que ele pertence, mas não às suas subclasses ou aos seus clientes.
C) Um atributo ou método privado é visível somente às subclasses da classe a que ele pertence.
D) Um método protegido não pode acessar os atributos privados declarados na classe a que ele pertence, sendo necessária a chamada de outro método privado da classe.
E) Um método público pode acessar somente atributos públicos declarados na classe a que ele pertence.

Resposta:
Um atributo ou método protegido é visível também às classes e subclasses que pertencem ao mesmo pacote, assim a alternativa "B" está errada.
Um atributo ou método privado é visível somente à classe que ele pertence, portanto a alternativa "C" está errada.
Um método protegido pode acessar os atributos privados declarados na classe a que ele pertence, logo a alternativa "D".
Um método publico pode acessar os atributos declarados na classe a que ele pertence, portanto a alternativa "E" está errada.
Como um atributo ou método público é visível a qualquer classe cliente e subclasse da classe a que ele pertence, então a alternativa correta é a letra "A".

*********************

POSCOMP 2013 - QUESTÃO 31
Entre as linguagens de programação mais comumente encontradas nas mais diversas aplicações, tem-se Java e C++. Sobre essas duas linguagens, atribua V (verdadeiro) ou F (falso) às afirmativas a seguir.

( ) A linguagem C++ é uma extensão da linguagem C.
( ) A linguagem Java é interpretada e C++ é compilada.
( ) A linguagem Java possui tratamento de exceções.
( ) Ambas possuem tipagem dinâmica.
( ) O coletor de lixo de Java é automático e o de C++ é manual.

Assinale a alternativa que contém, de cima para baixo, a sequência correta.

A) V, V, F, F, V.
B) V, F, V, F, V.  ***
C) V, F, F, V, F.
D) F, V, V, V, F.
E) F, F, F, V, V.

Resposta: C++ é uma extensão de C. C++ é compilada, mas Java é hibrida, compilada e interpretada. Java tem tratamento de exceções. C++ e Java tem tipagem estática. C++ não possui coleta de lixo automática, ficando tudo a cargo do programador, enquanto Java possui. Portanto, a alternativa correta é a letra "B".

*********************

POSCOMP 2014 - QUESTÃO 28
Considere as classes Java, que pertencem ao mesmo pacote, a seguir.

abstract public class C1
{
    abstract public Object cria();

    public void mostra()
    {
        System.out.print("Poscomp 2014");
    }
}

public class C2 extends C1
{
    static int i = 0;
    Integer j;
    public Object cria()
    {
        i++;
        j = new Integer(i);
        return j;
    }
    public void mostra()
    {
        System.out.print("j=" + j);
    }
}

public class C3 extends C1
{
    double d = 3.14;
    Float f;
    public Object cria()
    {
        d = d + 1.0;
        f = new Float(d);
        return f;
    }
    public void mostra()
    {
        System.out.print("f=" + f);
    }
}

public class PosComp2014
{
    public static void main(String[] z)
    {
        C1 a,b,c;
        Object o1,o2,o3;
        a = new C2();
        b = new C2();
        c = new C3();
        o1 = a.cria();
        o1 = a.cria();
        o2 = b.cria();
        o3 = c.cria();
        o3 = c.cria();
        a.mostra();
        System.out.print(" ");
        b.mostra();
        System.out.print(" ");
        c.mostra();
        System.out.print(" " + o1);
        System.out.print(" " + o2);
        System.out.print(" " + o3);
    }
}

Assinale a alternativa que apresenta, corretamente, os valores impressos pela execução desse programa.

A) O programa está sintaticamente incorreto, não sendo possível executá-lo.
B) j=2 j=1 f=5.14 2 1 5.14
C) j=2 j=3 f=5.14 2 3 5.14        ***
D) Poscomp 2014 Poscomp 2014 Poscomp 2014 2 1 5.14
E) Poscomp 2014 Poscomp 2014 Poscomp 2014 2 3 5.14

Resposta: sintaticamente o programa está correto. Tem que se notar o uso de polimorfismo. Tem que se notar também que as subclasses C2 e C3 tem contadores de uso do método cria, iniciando, respectivamente, em 0 e 3.14, sendo que em C2 é static. Assim, a.j=2, b.j=3, c.f=5.14, o1=2, o2=3 e o3=5.14. Portanto, será impresso o seguinte: j=2 j=3 f=5.14 2 3 5.14. A Alternativa correta é a letra "C".

*********************

POSCOMP 2015 - QUESTÃO 28
Considere o seguinte código desenvolvido em Java.

public class Animal
{
    int numeroPatas;
    public void fale (){};
}

public class Cao extends Animal
{
    public void fale()
    {
        System.out.println ("au au");
    }
}

public class Gato extends Animal
{
    public void fale()
    {
        System.out.println ("miau");
    }
}

public class GatoPersa extends Gato
{
    public void fale()
    {
        System.out.println ("miauuuu");
    }
}

public class Tigre extends Gato
{
    public void fale()
    {
        super.fale();
        System.out.println ("rrrrrr");
    }
}

public class Principal
{
    public static void main(String[] args)
    {
        Gato gato = new GatoPersa();
        gato.fale();
        Cao cao = new Cao();
        cao.fale();
        Tigre tigre = new Tigre();
        tigre.fale();
    }
}

Ao executar o código, a saída impressa no console é:

A)  miauuuu         ***
    au au
    miau
    rrrrrr
B)  miauuuuu
    au au
    rrrrrr
C)  miau
    au au
    miau
    miau
D)  miau
    au au
    rrrrrr
E) miau
    au au
    miau
    rrrrrr

Resposta: além do conceito de herança, atentar para o uso de "super". Ele fará com que antes do rugido do tigre seja impresso um miado de gato. Assim, será impresso no console um miado de gato persa, um latido, um miado de gato e o rugido do tigre. Portanto, a alternativa correta é a letra "A"

*********************

POSCOMP 2015 - QUESTÃO 33
O conceito de encapsulamento de programação orientada a objetos pode ser implementado na linguagem Java por meio de

A) métodos estáticos (static) e públicos (public).
B) métodos públicos (public), privados (private) e protegidos (protected).     ***
C) classes abstratas (abstract) e métodos protegidos (protect).
D) interfaces (interface), métodos públicos (public) e métodos protegidos (protect).
E) herança (extends) e métodos estáticos (static).

Resposta: encapsulamento consiste no processo de ocultação dos detalhes internos de implementação de um objeto. Isso é feito por meio de métodos públicos (public), privados (private) e protegidos (protected). Alternativa correta é letra "B".

*********************

POSCOMP 2016 - QUESTÃO 28
Assinale a alternativa que apresenta o nome de uma linguagem de tipagem dinâmica.

A) Java.
B) C.
C) Python.  ***
D) Pascal.
E) C#.

Resposta: Java, C, C# e Pascal tem tipagem estática (exigem declarações de tipos de dados) enquanto que Python tem tipagem dinâmica, portanto a alternativa correta é a letra "C".

*********************

POSCOMP 2017 - QUESTÃO 70
Requisitos não funcionais de software são aqueles que não dizem respeito às funções específicas de software, mas, sim, a propriedades que o sistema deve possuir, ou restrições que deve atender. Existem diferentes tipos de requisitos funcionais. Abaixo estão listados exemplos para diferentes tipos de requisitos não funcionais:

 ________________: o software deve ser desenvolvido utilizando a linguagem de programação Java versão 7.4.
 ________________: deve ser possível acessar o sistema a partir dos browsers Chrome, Internet Explorer e Safari.
 ________________: o sistema deve extrair os tweets da plataforma Tweeter utilizando a API REST disponível para este fim (detalhes de acesso à API em www.tweeter.com/API).

Assinale a alternativa que preenche, correta e respectivamente, as lacunas do trecho acima.

A) Desempenho – Portabilidade – Padrões
B) Eficiência – Padrões – Portabilidade
C) Implementação – Interoperabilidade – Portabilidade
D) Implementação – Portabilidade – Interoperabilidade   ***
E) Eficiência – Padrões – Interoperabilidade

Resposta: Um sistema acessível a partir de vários browsers tem que ser portável. Determinar a linguagem de desenvolvimento é requisito para a implementação do sistema. Possuir a capacidade de se comunicar com outro sistema é ser interoperável. Assim, a alternativa correta é a letra "D".

*********************

POSCOMP 2017 - QUESTÃO 28
De acordo com o diagrama de classes UML a seguir, assinale a alternativa que se relaciona diretamente com o conceito de polimorfismo da programação orientada a objetos.

OBS: ver imagem do diagrama no link: https://github.com/lokijuhygb/azsx/blob/master/extras/diagrama-classes-uml.png

A) A relação entre as classes “Livro” e “Capítulo”.
B) Os atributos “número: int” e “númeroDePágs: int” da classe “Capítulo”.
C) O método “ImprimeNome” das classes “Artefato” e “Livro”.     ***
D) O atributo “autor: String” da classe “Livro”.
E) A relação entre as classes “Capítulo” e “Página”.

Resposta: como polimorfismo é o princípio pelo qual duas ou mais classes derivadas de uma mesma superclasse podem invocar métodos que têm a mesma identificação (assinatura) mas comportamentos distintos, especializados para cada classe derivada, usando para tanto uma referência a um objeto do tipo da superclasse, a alternativa que se relaciona com o conceito é a letra "C".
