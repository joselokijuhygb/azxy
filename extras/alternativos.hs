import Data.Char -- Usado no item 6.

--t1

-- 08. Escreva uma função lastName :: String -> String que, dado o nome completo de uma pessoa, obtenha seu último sobrenome. Suponha que cada parte do nome seja separada por um espaço e que não existam espaços no início ou fim do nome.

lastName :: String -> String
lastName x = last (splitName ' ' x)

lastName1 :: String -> String
lastName1 x = last $ splitName ' ' x

lastName2 :: String -> String
lastName2 x = last . splitName ' ' $ x


-- 09. Escreva uma função userName :: String -> String que, dado o nome completo de uma pessoa, crie um nome de usuário (login) da pessoa, formado por: primeira letra do nome seguida do sobrenome, tudo em minúsculas. Dica: estude as funções pré-definidas no módulo Data.Char, para manipulação de maiúsculas e minúsculas. Você precisará carregar este módulo usando import Data.Char no interpretador ou no início do arquivo do programa.

lowerString :: String -> String
lowerString str = [ toLower loweredString | loweredString <- str]

lowerString1 :: String -> String
lowerString1 = map toLower

lowerString2 :: String -> String
lowerString2 = \x -> map toLower x

userName :: String -> String
userName x = lowerString (showLitChar (head $ firstName x) (lastName x))

userName2 :: String -> String
userName2 x = lowerString ((show $ head $ firstName x) ++ (lastName x))



--t2
-- 1. Usando recursão, escreva uma função geraTabela :: Int -> [(Int,Int)] que produza uma lista com n tuplas, cada tupla com números de n a 1 e seus respectivos quadrados.

-- Com recursão:
geraTabela :: Int -> [(Int,Int)]
geraTabela 1 = [(1,1)]
geraTabela x = (x,x^2):geraTabela (x-1)

-- Sem recursão:
geraTabela2 :: Int -> [(Int,Int)]
geraTabela2 x = [(p,p*p) | p <- [x,(x-1)..1]]


-- 2. Defina uma função recursiva que verifique se um dado caracter está contido numa string.

-- Com recursão e sem x:xs:
contido :: Char -> String -> Bool
contido c [] = False
contido c str = if c == head str then True else contido c (tail str)

-- Com recursão e com x:xs:
contido2 :: Char -> String -> Bool
contido2 c [] = False
contido2 c (x:xs) = if c == x then True else contido2 c xs

--Sem recursão:
contido3 :: Char -> String -> Bool
contido3 c str = not . null $ filter (\y -> y == c) str


-- 3. Defina uma função recursiva que receba uma lista de coordenadas de pontos 2D e desloque esses pontos em 2 unidades.

-- Com recursão
translate :: [(Float,Float)] -> [(Float,Float)]
translate [] = []
translate (x:xs) = (fst (x) + 2, snd (x) + 2):translate (xs)

-- Sem recursão
translate2 :: [(Float,Float)] -> [(Float,Float)]
translate2 lst = [(a+2,b+2) | (a,b) <- lst]

-- 4. Defina uma função que receba um número n e retorne uma lista de n tuplas, cada tupla com números de 1 a n e seus respectivos quadrados. Dica: defina uma função auxiliar recursiva com 2 argumentos, sendo um deles que se mantém inalterado na chamada recursiva.

-- Com recursão
geraTabela3aux :: Int -> Int -> [(Int,Int)]
geraTabela3aux 0 y = []
geraTabela3aux x y = (y-x+1,(y-x+1)^2):geraTabela3aux (x-1) y

geraTabela3 :: Int -> [(Int,Int)]
geraTabela3 x = geraTabela3aux x x

geraTabela4 :: Int -> [(Int,Int)]
geraTabela4 x = reverse $ geraTabela x

-- Sem recursão:
geraTabela5 :: Int -> [(Int,Int)]
geraTabela5 x = [(p,p*p) | p <- [1..x]]





-- 1. Escreva uma função recursiva isBin :: String -> Bool para verificar se uma dada String representa um número binário, ou seja, contém apenas caracteres '0' ou '1'. As únicas funções pré-definidas autorizadas aqui são head e tail.

isBin :: String -> Bool
isBin [] = True
isBin str = if (head str) == '0' || (head str) == '1' then isBin (tail str) else False

isBin2 :: String -> Bool
isBin2 [] = True
isBin2 (x:xs) = if x == '0' || x == '1' then isBin2 xs else False

isBin3 :: String -> Bool
isBin3 [] = True
isBin3 (x:xs) = if elem x "01" then isBin3 xs else False


-- 2. Reescreva a função acima de forma não-recursiva. Dê outro nome para ela, por exemplo isBin'. Aqui você pode usar quaisquer funções auxiliares pré-definidas em Haskell.

isBin4 :: String -> Bool
isBin4 str = null $ filter (\y -> y /= '0' && y /= '1') str

isBin5 :: String -> Bool
isBin5 str = null [x | x <- str, notElem x "01" == True]


-- 6. Implemente uma dessas funções: isHex :: String -> Bool ou hex2dec :: String -> Int ou dec2hex :: Int -> String, que são semelhantes às dos exercícios anteriores, porém com números hexadecimais no lugar de números binários. Aqui está tudo liberado: você pode escolher qual das funções irá implementar, sem restrições sobre como deve fazer isso.

binhex2dec :: [Int] -> Int -> Int
binhex2dec [] x = undefined
binhex2dec bits y = sum $ zipWith (*) bits [y^x | x <- [length bits -1,length bits -2..0]]

dec2aux :: Int -> Int -> [Int]
dec2aux x y
    | x < y = [x]
    | x >= y = (x `mod` y):dec2aux (x `div` y) y

dec2bin :: Int -> [Int]
dec2bin x = reverse $ dec2aux x 2

isHex :: String -> Bool
isHex str = null $ filter (\y -> not $ elem y "0123456789abcdefABCDEF") str

dec2hex :: Int -> String
dec2hex x = map ((['0'..'9'] ++ ['a'..'z']) !!) (reverse $ dec2aux x 16)

hex2dec :: String -> Int
hex2dec str = binhex2dec (map digitToInt str) 16



-- A codificação EAN-13 é um padrão de código de barras usado em vários tipos de produtos. O número codificado em barras tem 13 dígitos (0-9), sendo o último um dígito verificador (exemplo: 5901234123457, dígito verificador 7). O cálculo do dígito verificador obedece a algumas regras simples disponíveis em: https://www.gs1.org/services/how-calculate-check-digit-manually

-- Você deverá implementar uma função isEanOk :: String -> Bool, que verifique se uma dada string representa um número EAN-13 com dígito verificador válido.

import Data.Char

eanDV :: [Int] -> Int
eanDV digits = 10 - ((sum $ zipWith (*) digits [1,3,1,3,1,3,1,3,1,3,1,3]) `mod` 10)

isEanOk :: String -> Bool
isEanOk str = (eanDV $ map digitToInt str) == digitToInt (last str)


isEanOk2 :: String -> Bool
isEanOk2 str = 10 - ((sum $ zipWith (*) (map digitToInt str) (take 12 (cycle [1,3]))) `mod` 10) == digitToInt (last str)


